About
-----
tclexecomp is a TCL exectutable wrapper, which creates a single executable 
file from a tcl script. Currently it can create binaries for Linux (64 Bit), 
Windows (64 Bit) and MACOS (64 Bit). 
It is based on freewrap (http://freewrap.sourceforge.net/), but with many more 
tcl-modules integrated and support for more operation systems.

Main Features are
- Create executable Binaries from an TclTk-Script for Linux, Windows and MacOS
- Compile a Tcl program to bytecode
- Customize the Binaries to the needed modules of the Application

You can also use it as a normal Tcl/Tk-Interpreter.



Web-Site and Download
---------------------
You can find it under

  https://codeberg.org/karl69/tclexecomp/releases


Usage
-----
To wrap a Tcl/Tk-Script into an Executable, simply use the following command:
  tclexecomp <tclfile> -w <tclexecomp destination binary> -o <output-file> -forcewrap

As an example on Linux (64Bit) you can create an executable for Windows 64 (Bit) 
in the following way
  tclexecomp64 test.tcl -w tclexecomp64.exe -forcewrap -o test.exe

You can customize your binaries by calling
  tclexecomp64 -gui


Notes on integrated Modules
---------------------------
To use the integrated modules you have to do the usual
  package require ...
commands.


More informations on http://tclexecomp.sourceforge.net

You can also find the source code in the git repository on sourceforge.

best regards,
  Michael
