ROOTDIR = /home/michael/entwicklung/tclexecomp.sourceforge/src/..
PLATFORMDIR = /home/michael/entwicklung/tclexecomp.sourceforge/src/../linux64
PREFIX = $(PLATFORMDIR)/buildenv
ZIP = ../utils/zip

PROG = tclexecomp64
PROGREV = 1.4.0
TCLVERSIONDOT = 8.6
TCLSRCVERSION = 8.6.13
TKSRCVERSION = 8.6.13

include ../Makefile_linux64.inc

SCRIPT_TCL = $(PLATFORMDIR)/buildenv/lib/tcl8.6/tclIndex
ZSCRIPT_TCL = $(PLATFORMDIR)/zipdir/tcl8.6/tclIndex
SCRIPT_TK = $(PLATFORMDIR)/buildenv/lib/tk8.6/tclIndex
ZSCRIPT_TK = $(PLATFORMDIR)/zipdir/tk8.6/tclIndex
