# Compiler Definitions

ARCH=i686-w64

export CC=$(ARCH)-mingw32-gcc -D__WIN32__
export LD=$(ARCH)-mingw32-ld
export AR=$(ARCH)-mingw32-ar
export RC=$(ARCH)-mingw32-windres
export RANLIB=$(ARCH)-mingw32-ranlib
export NM=$(ARCH)-mingw32-nm

MINGW_LIB_PATH = /usr/i686-w64-mingw32/sys-root/mingw/lib/

LFLAGS = -mwindows
STDLIBS = -lws2_32 -lgdi32 -lcomdlg32 -limm32 -lcomctl32 -lshell32 -luuid -lole32 -loleaut32 -luser32 -luserenv -ladvapi32 -lnetapi32 -lws2_32 -lwinspool
CFLAGS= -pipe -DSTATIC_BUILD

INCLUDEDIR   = /usr/$(ARCH)-mingw32/sys-root/mingw/include

TCLDIR       = $(PLATFORMDIR)/build/tcl$(TCLSRCVERSION)/win
TKDIR        = $(PLATFORMDIR)/build/tk$(TCLSRCVERSION)/win
ZIPOBJDIR    = $(PLATFORMDIR)/build/zip31c
RCSEARCHDIRS = $(TCLDIR)/../generic -I $(TKDIR)/../generic -I $(TKDIR)
RCDIR        = $(TKDIR)/rc
 
OPT = -I$(PLATFORMDIR)/buildenv/include
ZVFSOPT = -D_USE_32BIT_TIME_T

LIBS = \
  $(PLATFORMDIR)/buildenv/lib/libtk$(TCLVERSION).a \
  $(PLATFORMDIR)/buildenv/lib/libtcl$(TCLVERSION).a \
  $(PLATFORMDIR)/buildenv/lib/dde1.4/libtcldde14.a \
  $(PLATFORMDIR)/buildenv/lib/reg1.3/libtclreg13.a \
  $(PLATFORMDIR)/buildenv/lib/libtclstub$(TCLVERSION).a \
  $(PLATFORMDIR)/buildenv/lib/libz.a

OBJSZIP = $(PLATFORMDIR)/zipmain.o \
  $(ZIPOBJDIR)/crc32.o \
  $(ZIPOBJDIR)/crypt.o \
  $(ZIPOBJDIR)/deflate.o \
  $(ZIPOBJDIR)/fileio.o \
  $(ZIPOBJDIR)/globals.o \
  $(ZIPOBJDIR)/nt.o \
  $(ZIPOBJDIR)/trees.o \
  $(ZIPOBJDIR)/ttyio.o  \
  $(ZIPOBJDIR)/util.o \
  $(ZIPOBJDIR)/win32.o \
  $(ZIPOBJDIR)/win32zip.o \
  $(ZIPOBJDIR)/win32i64.o \
  $(ZIPOBJDIR)/zbz2err.o \
  $(ZIPOBJDIR)/zipfile.o \
  $(ZIPOBJDIR)/zipup.o

# All object code modules
#
OBJ = $(OBJSZIP) \
  $(PLATFORMDIR)/spritz.o \
  $(PLATFORMDIR)/freelib.o \
  $(PLATFORMDIR)/freewrap.o \
  $(PLATFORMDIR)/fwcrypt.o \
  $(PLATFORMDIR)/tkwinico.o \
  $(PLATFORMDIR)/freewrap.res.o \
  $(PLATFORMDIR)/zvfs.o

$(PLATFORMDIR)/zipmain.o : zipmain.c
	$(CC) $(CFLAGS) $(OPT) -I$(ZIPOBJDIR) -DUSE_ZIPMAIN -DWIN32 -DFORCE_WIN32_OVER_UNIX -DIZ_CRCOPTIM_UNFOLDTBL -DUSE_ZLIB -o $(PLATFORMDIR)/zipmain.o -c zipmain.c

$(PLATFORMDIR)/$(PROG)_bare$(EXE): $(OBJ) $(LIBS)
	$(CC) $(LFLAGS) -o $(PLATFORMDIR)/$(PROG)_bare$(EXE) $(OBJ) $(LIBS) $(STDLIBS)
	strip $(PLATFORMDIR)/$(PROG)_bare$(EXE)
	upx -9 --lzma $(PLATFORMDIR)/$(PROG)_bare$(EXE)

