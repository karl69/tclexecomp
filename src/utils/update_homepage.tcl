#!/usr/bin/tclsh

proc get_version {} {
  global version
  global tclversion

  set file [open Makefile.inc]
  while {[gets $file line] >= 0} {
    if [regexp "^PROGREV = (.*)" $line egal version] {}
    if [regexp "^TCLSRCVERSION = (.*)" $line egal tclversion] {}
  }
  close $file
}

proc get_module_list {} {
  global modinfo
  global modlink

  set modlist {}
  set file [open Makefile]
   while {[gets $file line] >= 0} {
    if [regexp "^MODULES = (.*)" $line egal list] {
      set modlist [concat $modlist [split $list " "]]
    }
    if [regexp "^MODULES_LINUX = (.*)" $line egal list] {
      set modlist [concat $modlist [split $list " "]]
    }
    if [regexp "^MODULES_WINDOWS = (.*)" $line egal list] {
      set modlist [concat $modlist [split $list " "]]
    }
  }
  close $file

  foreach mod $modlist {
    if [file exists "modules/$mod/doc.tcl"] {
      source "modules/$mod/doc.tcl"
    }
  }

  set modlink(Tcl/Tk) "http://www.tcl.tk"
  foreach module [array names tec_modules_desc] {
    set mod_name [string tolower [lindex $tec_modules_desc($module) 0]]
    set mod_version [lindex $tec_modules_desc($module) 2]
    set modinfo($mod_name) $mod_version
    set modlink($mod_name) [lindex $tec_modules_desc($module) 4]
  }
}

proc get_openssl_version {} {
  set file [open modules/tls/Makefile]
  while {[gets $file line] >= 0} {
    if [regexp "^OPENSSLVERSION = (.*)" $line egal version] {
      break
    }
  }
  close $file

  return $version
}

get_version
get_module_list

set opensslversion [get_openssl_version]

set file [open changes/mod_list_$version w]
puts $file "Version V$version"
puts $file "Date [clock format [clock scan now] -format "%d.%m.%Y"]"
puts $file "Tcl/Tk V$tclversion"
foreach mod_name [lsort [array names modinfo]] {
  if {$mod_name == "tcltls"} {
    puts $file "$mod_name V$modinfo($mod_name) (openssl V$opensslversion)"
  } else {
    puts $file "$mod_name V$modinfo($mod_name)"
  }
}
close $file

set outfile [open ../website.new/content/history/_index.md w]
puts $outfile "---
title: \"Releases\"
date: 2022-08-20T22:34:12+02:00
draft: false
weight: 2
---"

set nr 0
set lastfilename [lindex [lsort [glob -nocomplain -tails -dir changes *]] end]
foreach filename [lsort [glob -nocomplain -tails -dir changes *]] {
  incr nr
  set file [open changes/$filename]
  while {[gets $file line] >= 0} {
    if [regexp "^Version (.*)" $line egal version] {
      set out($version) {}
      continue
    }
    if [regexp "^Date (.*)" $line egal date] {
      lappend out($version) "**$date Release $version**"
      lappend out($version) ""
      continue
    }
    set mod_name [lindex $line 0]
    set mod_version [join [lrange $line 1 end] " "]

    if ![info exists h1($mod_name)] {
      if {$nr == 1} {
        lappend out($version) "- $mod_name $mod_version"
      } else {
        lappend out($version) "- **$mod_name $mod_version**"
      }
      set h1($mod_name) $mod_version
    } else {
      if {$mod_version != $h1($mod_name)} {
        lappend out($version) "- **$mod_name $mod_version**"
      } else {
        lappend out($version) "- $mod_name $mod_version"
      }
      set h1($mod_name) $mod_version

    }
  }
  lappend out($version) ""
}

foreach i [lsort -decreasing [array names out]] {
  puts $outfile [join $out($i) "\n"]
}

close $outfile
