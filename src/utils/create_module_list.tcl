#!/usr/bin/tclsh

proc get_version {} {
  global version
  global tclversion

  set file [open Makefile.inc]
  while {[gets $file line] >= 0} {
    if [regexp "^PROGREV = (.*)" $line egal version] {}
    if [regexp "^TCLSRCVERSION = (.*)" $line egal tclversion] {}
  }
  close $file
}

proc get_module_list {} {
  global modinfo
  global modlink

  set modlist {}
  set file [open Makefile]
   while {[gets $file line] >= 0} {
    if [regexp "^MODULES = (.*)" $line egal list] {
      set modlist [concat $modlist [split $list " "]]
    }
    if [regexp "^MODULES_LINUX = (.*)" $line egal list] {
      set modlist [concat $modlist [split $list " "]]
    }
    if [regexp "^MODULES_WINDOWS = (.*)" $line egal list] {
      set modlist [concat $modlist [split $list " "]]
    }
  }
  close $file

  foreach mod $modlist {
    if [file exists "modules/$mod/doc.tcl"] {
      source "modules/$mod/doc.tcl"
    }
  }

  set modlink(Tcl/Tk) "http://www.tcl.tk"
  foreach module [array names tec_modules_desc] {
    set mod_name [string tolower [lindex $tec_modules_desc($module) 0]]
    set mod_version [lindex $tec_modules_desc($module) 2]
    set modinfo($mod_name) $mod_version
    set modlink($mod_name) [lindex $tec_modules_desc($module) 4]
  }
}

proc get_openssl_version {} {
  set file [open modules/tls/Makefile]
  while {[gets $file line] >= 0} {
    if [regexp "^OPENSSLVERSION = (.*)" $line egal version] {
      break
    }
  }
  close $file

  return $version
}

get_version
get_module_list

set opensslversion [get_openssl_version]

set file [open changes/mod_list_$version w]
puts $file "Version V$version"
puts $file "Date [clock format [clock scan now] -format "%d.%m.%Y"]"
puts $file "Tcl/Tk V$tclversion"
foreach mod_name [lsort [array names modinfo]] {
  if {$mod_name == "tcltls"} {
    puts $file "$mod_name V$modinfo($mod_name) (openssl V$opensslversion)"
  } else {
    puts $file "$mod_name V$modinfo($mod_name)"
  }
}
close $file

set indexfile [open ../website/src/index.m4 w]
puts $indexfile "include(`common.m4')dnl
header(`tclexecomp - a TCL exectutable wrapper. Overview')dnl
body_begin
<p>
<b>tclexecomp</b> is a TCL exectutable wrapper, which creates a single executable file
from a tcl script.<br>
Currently it can create binaries for Linux (64 Bit), Windows (64 Bit) and MACOS (64 Bit).
</p>
<p>
It is based on freewrap (http://freewrap.sourceforge.net/), but has many more Tcl-Modules<br>
included and is regulary updated.
</p>
<p>
You can also use it as a normal Tcl/TK-Interpreter.
</p>"

set outfile [open ../website/src/history.m4 w]
puts $outfile "include(`common.m4')dnl"
puts $outfile "header(`tclexecomp - a TCL exectutable wrapper. History')dnl"
puts $outfile "body_begin"

set nr 0
set lastfilename [lindex [lsort [glob -nocomplain -tails -dir changes *]] end]
foreach filename [lsort [glob -nocomplain -tails -dir changes *]] {
  incr nr
  set file [open changes/$filename]
  while {[gets $file line] >= 0} {
    if [regexp "^Version (.*)" $line egal version] {
      set out($version) {}
      if {$filename == $lastfilename} {
        puts $indexfile "<p>Modules contained in $version:</p>\n<ul>"
      }
      file mkdir ../sourceforge/tclexecomp-$version
      set readme [open ../sourceforge/tclexecomp-$version/README.txt w]
      puts $readme "tclexecomp is a TCL exectutable wrapper, which creates a single executable file from a tcl script.
Currently it can create binaries for Linux (64 Bit), Windows (64 Bit) and MACOS (64 Bit).
It is based on freewrap (http://freewrap.sourceforge.net/), but with many more tcl-modules integrated.

Usage
-----
To wrap a Tcl/Tk-Script into an Executable, simply use the following command:
  tclexecomp <tclfile> -w <tclexecomp destination binary> -o <output-file> -forcewrap

As an example on Linux (64Bit) you can create an executable for Windows 64 (Bit) in the following way
  tclexecomp64 test.tcl -w tclexecomp64.exe -forcewrap -o test.exe

To change the Icon on an Windows-Binary or to customize the Binary call
  tclexecomp64 -gui

You can also compile a tcl-File with it:
  tclexecomp64 -compile test.tcl
\n"

      puts $readme "Release $version"
      puts $readme "--------------"
      continue
    }
    if [regexp "^Date (.*)" $line egal date] {
      lappend out($version) "<p><b>$date Release $version</b></p>"
      lappend out($version) "<ul>"
      continue
    }
    set mod_name [lindex $line 0]
    set mod_version [join [lrange $line 1 end] " "]
    if {$filename == $lastfilename} {
      puts $indexfile "    <li><a href=\"$modlink($mod_name)\">$mod_name $mod_version</a></li>"
    }
    puts $readme "- $mod_name $mod_version"

    if ![info exists h1($mod_name)] {
      if {$nr == 1} {
        lappend out($version) "    <li>$mod_name $mod_version</li>"
      } else {
        lappend out($version) "    <li><b>$mod_name $mod_version</b></li>"
      }
      set h1($mod_name) $mod_version
    } else {
      if {$mod_version != $h1($mod_name)} {
        lappend out($version) "    <li><b>$mod_name $mod_version</b></li>"
      } else {
        lappend out($version) "    <li>$mod_name $mod_version</li>"
      }
      set h1($mod_name) $mod_version

    }
  }
  lappend out($version) "</ul>"
  close $readme
}

foreach i [lsort -decreasing [array names out]] {
  puts $outfile [join $out($i) "\n"]
}

puts $outfile ""
puts $outfile "body_end"
close $outfile

puts $indexfile "</ul>"
puts $indexfile "body_end"
close $indexfile
