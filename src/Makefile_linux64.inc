# Compiler Definitions
X11_INCLUDES = -I/usr/X11R6/include
STDLIBS = -L/usr/lib64 -L/usr/X11R6/lib -lX11 -lXext -lXft -lXss -lfreetype -lfontconfig -lXrender -ldl -lm -lpthread
CC = gcc -DSTATIC_BUILD=1
CFLAGS= -pipe -fPIC -DSTATIC_BUILD $(X11_INCLUDES)

ZIPOBJDIR    = $(PLATFORMDIR)/build/zip31c
OPT = -I$(PLATFORMDIR)/buildenv/include

LIBS = \
  $(PLATFORMDIR)/buildenv/lib/libtk$(TCLVERSIONDOT).a \
  $(PLATFORMDIR)/buildenv/lib/libtcl$(TCLVERSIONDOT).a \
  $(PLATFORMDIR)/buildenv/lib/libz.a

OBJSZIP = $(PLATFORMDIR)/zipmain.o \
  $(ZIPOBJDIR)/zipfile.o \
  $(ZIPOBJDIR)/zipup.o \
  $(ZIPOBJDIR)/fileio.o \
  $(ZIPOBJDIR)/util.o \
  $(ZIPOBJDIR)/globals.o \
  $(ZIPOBJDIR)/crypt.o  \
  $(ZIPOBJDIR)/ttyio.o \
  $(ZIPOBJDIR)/unix.o \
  $(ZIPOBJDIR)/crc32.o \
  $(ZIPOBJDIR)/zbz2err.o \
  $(ZIPOBJDIR)/deflate.o \
  $(ZIPOBJDIR)/trees.o

# All object code modules
#
OBJ = $(OBJSZIP) \
  $(PLATFORMDIR)/spritz.o \
  $(PLATFORMDIR)/freelib.o \
  $(PLATFORMDIR)/freewrap.o \
  $(PLATFORMDIR)/fwcrypt.o \
  $(PLATFORMDIR)/zvfs.o

$(PLATFORMDIR)/zipmain.o : zipmain.c
	$(CC) $(CFLAGS) $(OPT) -I$(ZIPOBJDIR) -DUSE_ZIPMAIN -DUSE_ZLIB -DUNIX -DENABLE_ENTRY_TIMING -DUIDGID_NOT_16BIT -DLARGE_FILE_SUPPORT -DUNICODE_SUPPORT -DHAVE_DIRENT_H -DNO_PARAM_H -DHAVE_TERMIOS_H -o $(PLATFORMDIR)/zipmain.o -c zipmain.c

$(PLATFORMDIR)/$(PROG)_bare$(EXE):	$(OBJ) $(LIBS)
	$(CC) $(LFLAGS) -o $(PLATFORMDIR)/$(PROG)_bare$(EXE) $(OBJ) $(LIBS) $(STDLIBS)
	strip $(PLATFORMDIR)/$(PROG)_bare$(EXE)
	upx -9 --lzma $(PLATFORMDIR)/$(PROG)_bare$(EXE)
