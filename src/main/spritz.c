#include <tcl.h>
#include <string.h>

typedef struct SPRITZ_CTX {
  unsigned char x;
  unsigned char y;
  unsigned char s[256];
} SPRITZ_CTX;

/* #define TRACE trace */
#define TRACE 1 ? ((void)0) : trace

static void trace(const char *format, ...)
{
  va_list args;
  va_start(args, format);
  vfprintf(stderr, format, args);
  va_end(args);
}
static Tcl_ObjType rc4_type;

static void rc4_free_rep(Tcl_Obj *obj)
{
  SPRITZ_CTX *ctx = (SPRITZ_CTX *)obj->internalRep.otherValuePtr;
  TRACE("rc4_free_rep(%08x)\n", (long)obj);
  Tcl_Free((char *)ctx);
}

static void rc4_dup_rep(Tcl_Obj *obj, Tcl_Obj *dup)
{
  SPRITZ_CTX *ctx = (SPRITZ_CTX *)obj->internalRep.otherValuePtr;
  TRACE("rc4_dup_rep(%08x,%08x)\n", (long)obj, (long)dup);
  dup->internalRep.otherValuePtr = (SPRITZ_CTX *)Tcl_Alloc(sizeof(SPRITZ_CTX));
  memcpy(dup->internalRep.otherValuePtr, ctx, sizeof(SPRITZ_CTX));
  dup->typePtr = &rc4_type;
}

static void rc4_string_rep(Tcl_Obj* obj)
{
  SPRITZ_CTX *ctx = (SPRITZ_CTX *)obj->internalRep.otherValuePtr;
  Tcl_Obj* tmpObj;
  char* str;
  TRACE("rc4_string_rep(%08x)\n", (long)obj);
  /* convert via a byte array to properly handle null bytes */
  tmpObj = Tcl_NewByteArrayObj((unsigned char *)ctx, sizeof(SPRITZ_CTX));
  Tcl_IncrRefCount(tmpObj);

  str = Tcl_GetStringFromObj(tmpObj, &obj->length);
  obj->bytes = Tcl_Alloc(obj->length + 1);
  memcpy(obj->bytes, str, obj->length + 1);

  Tcl_DecrRefCount(tmpObj);
}

static int rc4_from_any(Tcl_Interp* interp, Tcl_Obj* obj)
{
  TRACE("rc4_from_any %08x\n", (long)obj);
  return TCL_ERROR;
}

static Tcl_ObjType rc4_type = {
  "rc4c", rc4_free_rep, rc4_dup_rep, rc4_string_rep, rc4_from_any
};

void swap (unsigned char *lhs, unsigned char *rhs) {
  unsigned char t = *lhs;
  *lhs = *rhs;
  *rhs = t;
}

static int spritz_init(ClientData cdata, Tcl_Interp *interp, int objc, Tcl_Obj *const objv[]) {
  SPRITZ_CTX *ctx;
  Tcl_Obj *obj;
  const unsigned char *k;
  int n = 0, i = 0, j = 0, keylen;

  if (objc != 2) {
      Tcl_WrongNumArgs(interp, 1, objv, "keystring");
      return TCL_ERROR;
  }

  k = Tcl_GetByteArrayFromObj(objv[1], &keylen);

  obj = Tcl_NewObj();
  ctx = (SPRITZ_CTX *)Tcl_Alloc(sizeof(SPRITZ_CTX));
  ctx->x = 0;
  ctx->y = 0;
  for (n = 0; n < 256; n++)
      ctx->s[n] = n;
  for (n = 0; n < 256; n++) {
      j = (k[i] + ctx->s[n] + j) % 256;
      swap(&ctx->s[n], &ctx->s[j]);
      i = (i + 1) % keylen;
  }

  if (obj->typePtr != NULL && obj->typePtr->freeIntRepProc != NULL)
      obj->typePtr->freeIntRepProc(obj);
  obj->internalRep.otherValuePtr = ctx;
  obj->typePtr = &rc4_type;
  Tcl_InvalidateStringRep(obj);
  Tcl_SetObjResult(interp, obj);
  return TCL_OK;
}

static int spritz_crypt(ClientData cdata, Tcl_Interp *interp, int objc, Tcl_Obj *const objv[]) {
  Tcl_Obj *resObj = NULL;
  SPRITZ_CTX *ctx = NULL;
  unsigned char *data, *res, x, y;
  int size, n, i;
  int k = 0;
  int z = 0;

  if (objc != 3) {
      Tcl_WrongNumArgs(interp, 1, objv, "key data");
      return TCL_ERROR;
  }

  if (objv[1]->typePtr != &rc4_type
    && rc4_from_any(interp, objv[1]) != TCL_OK) {
    return TCL_ERROR;
  }

  ctx = objv[1]->internalRep.otherValuePtr;
  data = Tcl_GetByteArrayFromObj(objv[2], &size);
  res = (unsigned char *)Tcl_Alloc(size);

  x = ctx->x;
  y = ctx->y;
  for (n = 0; n < size; n++) {
      x = (x + 1) % 256;
      y = (ctx->s[(ctx->s[x] + y) % 256] + k) %256;
      k = (x + k + ctx->s[y]) %256;
      swap(&ctx->s[x], &ctx->s[y]);
      z = ctx->s[(y + ctx->s[(x + ctx->s[(z+k) % 256]) %256]) % 256];
      res[n] = data[n] ^ ctx->s[z];
  }
  ctx->x = x;
  ctx->y = y;

  resObj = Tcl_NewByteArrayObj(res, size);
  Tcl_SetObjResult(interp, resObj);
  Tcl_Free((char*)res);

  return TCL_OK;
}

int Spritz_Init(Tcl_Interp *interp) {
  Tcl_Namespace *nsPtr; /* pointer to hold our own new namespace */

  if (Tcl_InitStubs(interp, TCL_VERSION, 0) == NULL) {
    return TCL_ERROR;
  }
  
  /* create the namespace */
  nsPtr = Tcl_CreateNamespace(interp, "spritz", NULL, NULL);
  if (nsPtr == NULL) {
      return TCL_ERROR;
  }
        
 /* just prepend the namespace to the name of the command.
    Tcl will now create the 'hello' command in the 'hello'
    namespace so it can be called as 'hello::hello' */
  Tcl_CreateObjCommand(interp, "spritz::spritz_init", spritz_init, NULL, NULL);
  Tcl_CreateObjCommand(interp, "spritz::spritz_crypt", spritz_crypt, NULL, NULL);
  Tcl_PkgProvide(interp, "spritz", "1.0");
  return TCL_OK;
}
