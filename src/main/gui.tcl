#console show

package require tablelist_tile

proc init_pictures {} {
  set ::pictures(tg1-folder.png) [image create photo -data \
"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAilJREFUeNqMU89rE0EYfTuz1m790ZJoQeghaqulXoSIp568CQpePOit4EHw4D8g
QkWo/0TxXFAIePEgxaMQBLXWH1VqW9NWC6HGkGyyOzPr901mN1lQcNjHzM5+7+173+541QXY4XmYo6mE/xsbSYLHvPATt0Mbp2auP7rXqn2CRzcjx04iGJuAkEN5qu+junjzYXZrTPZIHDo+hZHiaUTNPbR2P2P7
wytMla/l+F4QgDgiI2my4CCMVjBRF/7BUYydmUWnE6HZ6kATIyFXFrTm2pQnNDlwkInWyKBITEVoNEPs7jX6AgSuTXk+1abDCrALa00ItMMuCi6jcbOgGrpk0usbfNXvgTQ6hlaxC+chVjojpjM3gDiSVmUSeD0Y
wTdMsNYZGoo2mTgI7sGleXOXHs0Z6yAXQfUjaGkdcObeZ3YzCTTCkMSti1wEchDDuAiGeqDooXHENIJH8367zTF8+6J3m2A1q8jWtQOvldZ/jXBrvWjrmSfuLwEsEisM2f+AGtmDsg5sqwc+IQvU621bzzz+o4IH
T1D4HeJowk2zn7L3L3CTpJQ5eHRo7lQOP3uxgiXiBZxjmFB4u4GdyZdPm6XpC8OjxRMH+E1b39Z+vVmZX+WsxSOI0mat/8TO4jI+MpcF9hlffuD219Xq87X31cuF8Ymzk+culsjB5kIFN2anUb9aRjcVoNp+5wcP
ChEqFLuyXaud/75Vu0Imxmm7Tuj+61z/EWAAclGWg0KibYEAAAAASUVORK5CYII="]
}

proc centerWindow {top} {
  regexp {([0-9]*)x([0-9]*)[+-]([0-9]*)[+-]([0-9]*)} [wm geometry .] egal sizex sizey relx rely
  set new_sizex [winfo reqwidth $top]
  set new_sizey [winfo reqheight $top]
  set new_relx [expr int(($sizex - $new_sizex) / 2 + $relx)]
  set new_rely [expr int(($sizey - $new_sizey) / 2 + $rely)]
  wm geometry $top "+$new_relx\+$new_rely"
}

proc drawDialogOpenfile {filename_variable_name data_variable_name widget_options} {
  eval "set filename \[tk_getOpenFile $widget_options\]"

  # check if the "Abort-Button" was pressed, because in these case the variable $filename is empty
  if {$filename == {}} {
    return
  } 
  set [join [list $filename_variable_name] {}] $filename

  set f [open $filename r]
  fconfigure $f -translation binary
  set [join [list $data_variable_name] {}] [read -nonewline $f]
  close $f
}

proc bytesTotext {bytes} {
  if {$bytes == ""} {
    set bytes 0
  }
  set kbytes [expr $bytes / 1024.0]
  if {$kbytes > 10000} {
    set mbytes [expr $kbytes / 1024.0]
    if {$mbytes > 10000} {
      set gbytes [expr $mbytes / 1024.0]
      return [format "%.2f GB" $gbytes]
    }
    return [format "%.2f MB" $mbytes]
  } else {
    if {$kbytes < 1} {
      return [format "%.2f kB" [expr $bytes / 1024.0]]
    } else {
      return [format "%.2f kB" $kbytes]
    }
  }
}

proc replace_ico {} {
  package require ico
  package require imgtools
  
  set png $::pngfile
  set exe $::tclexecomp_binary
  
  if ![file exists $png] {
    tk_dialog .myDialog "Error" "PNG-File not found: $png" error 0 "Ok"
    return
  }
  if ![file exists $exe] {
    tk_dialog .myDialog "Error" "EXE-File not found: $exe" error 0 "Ok"
    return
  }
  
  set file [file tempfile icofile]
  close $file
  file delete $icofile
  set icofile "$icofile.ico"
  set icofile "/tmp/tcl_XrewmB.ico"
  file delete -force $icofile
  
  set nr 0
  set h "x"
  set img [image create photo -file $png]
  foreach i {16 32 48 128 256} {
    set imgtemp [image create photo]
    imgtools::scale $img "$i$h$i" $imgtemp
    incr nr
    ::ico::writeIcon $icofile $nr 24 $imgtemp
    image delete $imgtemp
  }
  
  set srcIconList [::ico::icons $icofile]
  set srcImgs [::ico::iconMembers $icofile $srcIconList]
  set destIconList [::ico::icons $exe]
  set destImgs [::ico::iconMembers $exe $destIconList]
  for {set i 0} {$i < 5} {incr i} {
    ::ico::copyIcon $icofile $i $exe [expr $i + 1]
  }
  file delete $icofile

  tk_dialog .myDialog "Info" "Icon's has been replaced in $exe" info 0 "Ok"
}

init_pictures

set hf [ttk::frame .hf]
set fo [ttk::frame $hf.fo]
set f_modules [ttk::labelframe $hf.f_modules -padding 5 -text "Included Modules"]
set f_ico [ttk::labelframe $hf.f_ico -padding 5 -text "Replace ICON in Windows EXE-File"]
  set f_ico_o [ttk::frame $f_ico.text]

pack $hf -side top -anchor w
pack $fo -side top -anchor w
pack $f_modules -side top -anchor w -padx 10 -pady 5
pack $f_ico -side top -anchor w -padx 10 -pady 5 -fill x
  pack $f_ico_o -side top -anchor w

ttk::label $fo.l1 -text "tclexecomp Binary"
grid $fo.l1 -column 0 -row 0 -sticky w -padx 10 -pady 3

ttk::frame $fo.f1
  ttk::entry $fo.f1.e1 -textvariable ::tclexecomp_binary -width 80
  pack $fo.f1.e1 -side left
  
  set ::tclexecomp_binary ""
  
  ttk::button $fo.f1.b1 -image $::pictures(tg1-folder.png) -command \
    "drawDialogOpenfile ::tclexecomp_binary ::tclexecomp_binary_data { \
     -filetypes {{{Binaries} {tclexecomp*}}} \
    }; fill_modules"
  pack $fo.f1.b1 -side left -padx 5
grid $fo.f1 -column 1 -row 0 -sticky wens -padx 10 -pady 3

ttk::label $fo.l2 -text "tclexecomp Version"
grid $fo.l2 -column 0 -row 1 -sticky w -padx 10 -pady 3
ttk::label $fo.e2 -textvariable ::tclexecomp_version -width 20
grid $fo.e2 -column 1 -row 1 -sticky wens -padx 10 -pady 3

ttk::label $fo.l3 -text "Binary Size"
grid $fo.l3 -column 0 -row 2 -sticky w -padx 10 -pady 3
ttk::label $fo.e3 -textvariable ::tclexecomp_binary_size -width 20
grid $fo.e3 -column 1 -row 2 -sticky wens -padx 10 -pady 3

set tablelist_modules $f_modules.tbl
tablelist::tablelist $tablelist_modules \
  -columns {0 "Nr"              left
            0 "Module"          left
            0 "Module"          left
            0 "Version"         right
            0 "Description"     left
            0 "Size"            right} \
  -height 15 -width 128 -stretch all -spacing 1 -selectmode extended

$tablelist_modules columnconfigure 0 -showlinenumbers 1
$tablelist_modules columnconfigure 1 -hide 1

pack $tablelist_modules -side top -anchor w -fill x -padx 10 -pady 10

ttk::button $f_modules.b1 -text "Delete chosen Modules from Binary" -command \
  [list delete_chosen_modules $tablelist_modules]
pack $f_modules.b1 -side top -anchor w -padx 10 -pady 10 

# Icon Widget
ttk::label $f_ico_o.l1 -text "ICON (PNG-File)"
grid $f_ico_o.l1 -column 0 -row 0 -sticky w -padx 10 -pady 3

ttk::frame $f_ico_o.f1
  ttk::entry $f_ico_o.f1.e1 -textvariable ::pngfile -width 80
  pack $f_ico_o.f1.e1 -side left
  
  set ::tclexecomp_binary ""
  
  ttk::button $f_ico_o.f1.b1 -image $::pictures(tg1-folder.png) -command \
    "drawDialogOpenfile ::pngfile ::pngfile_data { \
     -filetypes {{{PNG-Files} {*.png}}} \
    }"
  pack $f_ico_o.f1.b1 -side left -padx 5
grid $f_ico_o.f1 -column 1 -row 0 -sticky wens -padx 10 -pady 3

set ::pngfile ""
set replace_botton [ttk::button $f_ico.b1 -text "Replace Icon" -command replace_ico]
pack $f_ico.b1 -side left -padx 10 -pady 10

$::replace_botton configure -state "disabled"

proc delete_chosen_modules {tbl} {
  set rowlist [$tbl curselection]
  set dellist {}
  foreach row $rowlist {
    set h [$tbl get $row]
    lappend dellist [lindex $h 1]
  }
  if {$dellist != {}} {
    puts "exec [info nameofexecutable] cust.tcl -o $::tclexecomp_binary.temp -forcewrap -delmod [join $dellist ,] -w $::tclexecomp_binary"
    if [catch {exec [info nameofexecutable] cust.tcl -o $::tclexecomp_binary.temp -forcewrap -delmod [join $dellist ,] -w $::tclexecomp_binary} msg] {
      puts "exec [info nameofexecutable] cust.tcl -o $::tclexecomp_binary.temp -forcewrap -delmod [join $dellist ,] -w $::tclexecomp_binary"
      puts "Error: $msg"
    }
    file rename -force $::tclexecomp_binary.temp $::tclexecomp_binary
    fill_modules
  }
}

set h {proc fill_modules {} {
  set tablelist %%tablelist_modules%%
  $tablelist delete 0 end
  
  if {$::tclexecomp_binary == ""} {
    return
  }
  
  if [regexp {.exe$} $::tclexecomp_binary] {
    $::replace_botton configure -state "normal"
  } else {
    $::replace_botton configure -state "disabled"
  }
  set ::tclexecomp_binary_size [bytesTotext [file size $::tclexecomp_binary]]
  
  if {[catch {::zvfs::mount $::tclexecomp_binary /cust} msg]} {
   puts "Error: $msg"
  }

  set valuelist {}
  set file [open /cust/init_modules.tcl]
  while {[gets $file line] >= 0} {
    if [regexp "^set tec_modules_desc" $line] {
      eval $line
    }
  }
  close $file
  
  set file [open /cust/_freewrap_init.txt]
  while {[gets $file line] >= 0} {
    if [regexp "^tclexecomp64.* (.*)" $line egal version] {
      set ::tclexecomp_version $version
    }
  }
  close $file
  
  foreach module [array names tec_modules_desc] {
    set moddirname [lindex $tec_modules_desc($module) 0]
    set modname [lindex $tec_modules_desc($module) 1]
    set modversion [lindex $tec_modules_desc($module) 2]
    if [file exists /cust/modules/$moddirname$modversion] {
      set version "V$modversion"
      set desc [lindex $tec_modules_desc($module) 3]
      set size [bytesTotext [lindex $tec_modules_desc($module) 5]]
      lappend valuelist [list "" $moddirname $module $version $desc $size]
    }
  }
  
  ::zvfs::unmount $::tclexecomp_binary
  
  $tablelist insertlist end $valuelist
  
  $tablelist sortbycolumn 1 -increasing
}}
regsub -all {%%tablelist_modules%%} $h "$tablelist_modules" h
eval $h

fill_modules

centerWindow .
