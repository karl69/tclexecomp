#!/usr/bin/tclsh

console show

package require ico
package require imgtools

puts [pwd]
set png [lindex $argv 0]
set name [file rootname [file tail $png]]
set icofile "$name.ico"

if ![file exists $png] {
  puts "Error: $png does not exist"
  exit
}

set h "x"
file delete -force $icofile
file delete -force tmp
file mkdir tmp
set nr 0
set img [image create photo -file $png]
foreach i {16 32 48 128 256} {
  set imgtemp [image create photo] 
  imgtools::scale $img "$i$h$i" $imgtemp
  incr nr
  ::ico::writeIcon $icofile $nr 24 $imgtemp
}

set srcIconList [::ico::icons $icofile]
set srcImgs [::ico::iconMembers $icofile $srcIconList]
exit

