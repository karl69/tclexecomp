/*
# freeWrap is Copyright (c) 1998-2013 by Dennis R. LaBelle (labelled@nycap.rr.com)
# All Rights Reserved.
#
# This software is provided 'as-is', without any express or implied warranty. In no
# event will the authors be held #liable for any damages arising from the use of 
# this software. 
#
# Permission is granted to anyone to use this software for any purpose, including
# commercial applications, and to #alter it and redistribute it freely, subject to
# the following restrictions: 
#
# 1. The origin of this software must not be misrepresented; you must not claim 
#    that you wrote the original software. If you use this software in a product, an 
#    acknowledgment in the product documentation would be appreciated but is not
#    required. 
#
# 2. Altered source versions must be plainly marked as such, and must not be
#    misrepresented as being the original software. 
#
# 3. This notice may not be removed or altered from any source distribution.
*/

/*
** This file implements the main routine for a standalone TCL/TK shell.
   Revision history:

   Revison  Date           Author             Description
   -------  -------------  -----------------  ----------------------------------------------
     5.2    June 2, 2002   Dennis R. LaBelle  1) Changed mount of ZVFS to / instead of /zvfs
                                              2) Now appends to auto_path instead of
                                                 directly setting auto_path variable.

     5.4    Oct 16, 2002   Dennis R. LaBelle  1) Added necessary beginning space when appending
                                                 to auto_path TCL variable. This fixed problem
                                                 with locating the executable in a directory
                                                 path that included a space.
                                              2) Removed / from auto_path variable to prevent
                                                 searching of / path for libraries upon startup.

     5.5    Jan 12, 2003   Dennis R. LaBelle  1) Removed use of WinICO since Windows icon feature
                                                 is supported by "wm iconbitmap" command as of
                                                 TK 8.3.3

     5.6    Nov  9, 2003   Dennis R. LaBelle  1) Included WinICO again due to user requests
                                                 since WinICO includes features beyond those of
                                                 "wm iconbitmap".

     6.42   Jan 25, 2009   Dennis R. LaBelle  1) Added code to temporarily change the working
                                                 directory to C:/ under Windows in order to
                                                 prevent TK from traversing network directories at
                                                 startup. This significantly improves startup
                                                 time under Windows.

     6.5    Jan  2, 2009  Dennis R. LaBelle  1) Included BLT and tkpng extensions into freewrapPLUS

     6.6    Apr  5, 2013  Dennis R. LaBelle  1) Removed tkpng extension from freewrapPLUS since
                                                PNG support is now included in the core of TK 8.6.
                                             2) Adapted code to new TCL/TK 8.6 directory structure.
                                             3) Removed code supporting BLT since BLT is not
                                                compatible with TCL/TK 8.6.
                                                
     6.63   Apr  6, 2014  Dennis R. LaBelle  1) Modified code to define TK_LIBRARY variable in TCL
                                                instead of tk_library. This should speed up search for
                                                tk.tcl and improve application start up time.
*/
#include <tcl.h>

#ifndef WITHOUT_TK
#include <tk.h>
#endif

/* function prototypes */
extern int	isatty _ANSI_ARGS_((int fd));

/*
** We will be linking against all of these extensions.
*/
#if defined(__WIN32__)
#include <windows.h>
extern int Registry_Init(Tcl_Interp *);
extern int Dde_Init(Tcl_Interp *);
#endif

extern int Freewrap_Init(Tcl_Interp*);
extern int Spritz_Init(Tcl_Interp*);
extern int Winico_Init(Tcl_Interp*);
extern int Winico_SafeInit(Tcl_Interp*);
extern int Zvfs_Init(Tcl_Interp*);
extern int Zvfs_Mount(Tcl_Interp*, char*, char *);
EXTERN int TclObjCommandComplete _ANSI_ARGS_((Tcl_Obj * cmdPtr));

#define mountPt "/zvfs"
/*
** This routine runs first.  
*/
int main(int argc, char **argv){
  Tcl_Interp *interp;
  char *args;
  char buf[100];
  int tty;
  char TCLdir[20];
  char TKdir[20];
  char autopath[20];
  char sourceCmd[80];
  Tcl_DString appName;
  Tcl_Obj *argvPtr;

#ifdef WITHOUT_TK
    Tcl_Obj *resultPtr;
    Tcl_Obj *commandPtr = NULL;
    char buffer[1000];
    int code, gotPartial, length;
    Tcl_Channel inChannel, outChannel, errChannel;
#endif

  /* Create a Tcl interpreter
  */
  Tcl_FindExecutable(argv[0]);
  interp = Tcl_CreateInterp();
  if( Tcl_PkgRequire(interp, "Tcl", TCL_PATCH_LEVEL, 1)==0 ){
    return 1;
  }

  /* We have to initialize the virtual filesystem before calling
  ** Tcl_Init().  Otherwise, Tcl_Init() will not be able to find
  ** its startup script files.
  */

  Zvfs_Init(interp);
  Tcl_SetVar(interp, "extname", "", TCL_GLOBAL_ONLY);
  Zvfs_Mount(interp, (char *)Tcl_GetNameOfExecutable(), mountPt);
  sprintf(TCLdir, "%s/tcl8.6", mountPt);
  Tcl_SetVar2(interp, "env", "TCL_LIBRARY", TCLdir, TCL_GLOBAL_ONLY);

  tty = isatty(0);
  Tcl_SetVar(interp, "tcl_interactive", "0", TCL_GLOBAL_ONLY);

  /* Initialize Tcl and Tk
  */
  if( Tcl_Init(interp) ) {
	printf("Unable to initialize TCL.\n");
	return TCL_ERROR;
    }

  sprintf(autopath, " %s", TCLdir);
  Tcl_SetVar(interp, "auto_path", autopath, TCL_GLOBAL_ONLY | TCL_APPEND_VALUE);
  Tcl_SetVar(interp, "tcl_libPath", TCLdir, TCL_GLOBAL_ONLY);
  Tcl_SetVar(interp, "tcl_library", TCLdir, TCL_GLOBAL_ONLY);

  /* We need to re-init encoding (after initializing Tcl),
     otherwise "encoding system" will return "identity"
  */
  TclpSetInitialEncodings();

  /* ... and preserve original encoding system, just in case :)
  */
  sprintf(sourceCmd, "set origSysEncoding [encoding system]");
  Tcl_Eval(interp, sourceCmd);

  /* Now we have to convert commandline from local encoding to UTF8  */
  /* The following block of code was copied from Tk_MainEx procedure */

  /**[ begin ]***************/
  Tcl_ExternalToUtfDString(NULL, argv[0], -1, &appName);
  Tcl_SetVar(interp, "argv0", Tcl_DStringValue(&appName), TCL_GLOBAL_ONLY);
  Tcl_DStringFree(&appName);
  argc--;
  argv++;

  Tcl_SetVar2Ex(interp, "argc", NULL, Tcl_NewIntObj(argc), TCL_GLOBAL_ONLY);

  argvPtr = Tcl_NewListObj(0, NULL);
  while (argc--) {
      Tcl_DString ds;

      Tcl_ExternalToUtfDString(NULL, *argv++, -1, &ds);
      Tcl_ListObjAppendElement(NULL, argvPtr, Tcl_NewStringObj(Tcl_DStringValue(&ds), Tcl_DStringLength(&ds)));
      Tcl_DStringFree(&ds);
  }
  Tcl_SetVar2Ex(interp, "argv", NULL, argvPtr, TCL_GLOBAL_ONLY);
  /**[ end ]*****************/

#ifdef WITHOUT_TK
  Tcl_SetVar(interp, "extname", "TCLSH", TCL_GLOBAL_ONLY);
#else
  sprintf(TKdir, "%s/tk8.6", mountPt);
  Tcl_SetVar2(interp, "env", "TK_LIBRARY", TKdir, TCL_GLOBAL_ONLY);
  Tcl_SetVar(interp, "TK_LIBRARY", TKdir, TCL_GLOBAL_ONLY);

#if defined(__WIN32__)
  sprintf(sourceCmd, "set orgDir [pwd]; catch {cd C:/}");
  Tcl_Eval(interp, sourceCmd);
#endif
  
  if ( Tk_Init(interp) ) {
       printf("Unable to initialize TK\n");
       return TCL_ERROR;
    }

  Tcl_StaticPackage(interp,"Tk", Tk_Init, 0);

  Tk_InitConsoleChannels(interp);
  /* Make up for a deficiency in the Tk_InitConsoleChannels function */
  Tcl_RegisterChannel(interp, Tcl_GetStdChannel(TCL_STDIN));
  Tcl_RegisterChannel(interp, Tcl_GetStdChannel(TCL_STDOUT));
  Tcl_RegisterChannel(interp, Tcl_GetStdChannel(TCL_STDERR));
  /* End of fix */
  
  Tk_CreateConsoleWindow(interp);

#if defined(__WIN32__)
  sprintf(sourceCmd, "cd $orgDir; unset orgDir");
  Tcl_Eval(interp, sourceCmd);
#endif

#endif

  /* Start up all extensions.
  */

  if (Spritz_Init(interp) == TCL_ERROR) {
    return TCL_ERROR;
  }
  Tcl_StaticPackage(interp, "spritz", Spritz_Init, 0);

#if defined(__WIN32__)
  /* DRL - Do the standard Windows extentions */

  if (Registry_Init(interp) == TCL_ERROR) {
      return TCL_ERROR;
     }
  Tcl_StaticPackage(interp, "registry", Registry_Init, 0);

  if (Dde_Init(interp) == TCL_ERROR) {
      return TCL_ERROR;
     }
  Tcl_StaticPackage(interp, "dde", Dde_Init, 0);
#endif

#if !defined(WITHOUT_TK) && !defined(WITHOUT_WINICO) && (defined(__WIN32__) || defined(_WIN32))
  if (Winico_Init(interp) == TCL_ERROR) return TCL_ERROR;

  Tcl_StaticPackage(interp, "winico", Winico_Init, Winico_SafeInit);
#endif

   /* Add some freeWrap commands */
  if (Freewrap_Init(interp) == TCL_ERROR) return TCL_ERROR;

  sprintf(sourceCmd, "source %s/init_modules.tcl", mountPt);
  Tcl_Eval(interp, sourceCmd);

  /* After all extensions are registered, start up the
  ** program by running freewrapCmds.tcl.
  */
  sprintf(sourceCmd, "source %s/freewrapCmds.tcl", mountPt);
  Tcl_Eval(interp, sourceCmd);
  sprintf(sourceCmd, "source %s/spritz.tcl", mountPt);
  Tcl_Eval(interp, sourceCmd);
  // if (Tcl_Eval(interp, sourceCmd) != TCL_OK) { printf("Error executing: %s\n", sourceCmd); }

#ifndef WITHOUT_TK
    /*
     * Loop infinitely, waiting for commands to execute.  When there
     * are no windows left, Tk_MainLoop returns and we exit.
     */

    Tk_MainLoop();
    Tcl_DeleteInterp(interp);
    Tcl_Exit(0);
#else
    /*
     * Process commands from stdin until there's an end-of-file.  Note
     * that we need to fetch the standard channels again after every
     * eval, since they may have been changed.
     */
    commandPtr = Tcl_NewObj();
    Tcl_IncrRefCount(commandPtr);

    inChannel = Tcl_GetStdChannel(TCL_STDIN);
    outChannel = Tcl_GetStdChannel(TCL_STDOUT);
    gotPartial = 0;
    while (1) {
	if (tty) {
	    Tcl_Obj *promptCmdPtr;

	    promptCmdPtr = Tcl_GetVar2Ex(interp,
		    (gotPartial ? "tcl_prompt2" : "tcl_prompt1"),
		    NULL, TCL_GLOBAL_ONLY);
	    if (promptCmdPtr == NULL) {
                defaultPrompt:
		if (!gotPartial && outChannel) {
		    Tcl_WriteChars(outChannel, "% ", 2);
		}
	    } else {
		code = Tcl_EvalObjEx(interp, promptCmdPtr, 0);
		inChannel = Tcl_GetStdChannel(TCL_STDIN);
		outChannel = Tcl_GetStdChannel(TCL_STDOUT);
		errChannel = Tcl_GetStdChannel(TCL_STDERR);
		if (code != TCL_OK) {
		    if (errChannel) {
			Tcl_WriteObj(errChannel, Tcl_GetObjResult(interp));
			Tcl_WriteChars(errChannel, "\n", 1);
		    }
		    Tcl_AddErrorInfo(interp,
			    "\n    (script that generates prompt)");
		    goto defaultPrompt;
		}
	    }
	    if (outChannel) {
		Tcl_Flush(outChannel);
	    }
	}
	if (!inChannel) {
	    goto done;
	}
        length = Tcl_GetsObj(inChannel, commandPtr);
	if (length < 0) {
	    goto done;
	}
	if ((length == 0) && Tcl_Eof(inChannel) && (!gotPartial)) {
	    goto done;
	}

        /*
         * Add the newline removed by Tcl_GetsObj back to the string.
         */

	Tcl_AppendToObj(commandPtr, "\n", 1);
	if (!TclObjCommandComplete(commandPtr)) {
	    gotPartial = 1;
	    continue;
	}

	gotPartial = 0;
	code = Tcl_RecordAndEvalObj(interp, commandPtr, 0);
	inChannel = Tcl_GetStdChannel(TCL_STDIN);
	outChannel = Tcl_GetStdChannel(TCL_STDOUT);
	errChannel = Tcl_GetStdChannel(TCL_STDERR);
	Tcl_DecrRefCount(commandPtr);
	commandPtr = Tcl_NewObj();
	Tcl_IncrRefCount(commandPtr);
	if (code != TCL_OK) {
	    if (errChannel) {
		Tcl_WriteObj(errChannel, Tcl_GetObjResult(interp));
		Tcl_WriteChars(errChannel, "\n", 1);
	    }
	} else if (tty) {
	    resultPtr = Tcl_GetObjResult(interp);
	    Tcl_GetStringFromObj(resultPtr, &length);
	    if ((length > 0) && outChannel) {
		Tcl_WriteObj(outChannel, resultPtr);
		Tcl_WriteChars(outChannel, "\n", 1);
	    }
	}
    }

    /*
     * Rather than calling exit, invoke the "exit" command so that
     * users can replace "exit" with some other command to do additional
     * cleanup on exit.  The Tcl_Eval call should never return.
     */

    done:
    if (commandPtr != NULL) {
	Tcl_DecrRefCount(commandPtr);
    }
    sprintf(buffer, "exit %d", 0);
    Tcl_Eval(interp, buffer);

#endif

  return TCL_OK;
}

