namespace eval ::spritz {
  namespace export spritz
}

proc ::spritz::Pop {varname {nth 0}} {
  upvar $varname args
  set r [lindex $args $nth]
  set args [lreplace $args $nth $nth]
  return $r
}

proc ::spritz::spritz {args} {
  array set opts {-hex 0 -infile {} -in {} -out {} -chunksize 4096 -key {} -command {}}
  while {[string match -* [set option [lindex $args 0]]]} {
    switch -exact -- $option {
      -key        { set opts(-key) [Pop args 1] }
      default {
        if {[llength $args] == 1} { break }
        if {[string compare $option "--"] == 0} { Pop args; break }
        set err [join [lsort [array names opts]] ", "]
        return -code error "bad option $option:\
          must be one of $err"
      }
    }
    Pop args
  }

  if {[string length $opts(-key)] < 1} {
    return -code error "wrong # args: should be \"spritz -key key\""
  }

  if {[llength $args] != 1} {
    return -code error "wrong # args:\
      should be \"rc4 ?-hex? -key key -in channel | string\""
  }

  set Key [::spritz::spritz_init $opts(-key)]
  set r [::spritz::spritz_crypt $Key [lindex $args 0]]

  return $r
}
    
package provide spritz 1.0

