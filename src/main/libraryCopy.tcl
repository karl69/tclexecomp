#!/usr/bin/tclsh
## Program: libraryCopy.tcl
#
# This file performs the following functions while building the freeWrap application:
#     1) Copy the TCL/TK standard scripts to the specified destination
#     2) Add the standard TCL library directories to the tcl_pkgPath variable
#     3) Generate pkgIndex.tcl files so that TCL modules can be loaded with [package require]
#
# Revison  Date           Author             Description
# -------  -------------  -----------------  ----------------------------------------------
#   6.4    Jan. 20, 2008  Dennis R. LaBelle  1) This file replaced the shrink.tcl script.
#                                               Extra white space is no longer removed from
#                                               the copied TCL scripts.
#
#   6.41   Feb. 17, 2008  Dennis R. LaBelle  1) Modified code so that TCL package paths are
#                                               added to tcl_pkgPath variable instead of
#                                               auto_path.
#                                            2) Added code to copy the TCL module directories.
#                                            3) Added code to generate pkgIndex.tcl files
#                                               so that TCL modules can be loaded with a
#                                               [package require] command.
#
#   6.6    May  22, 2013  Dennis R. LaBelle  1) Modified code to support additional TCL
#                                               package directories present in TCL 8.6. as well
#                                               as directories for tcllib and tklib.
#                                            2) Removed code which generated pkgIndex.tcl files
#                                               for TCL modules. Instead, paths to the modules
#                                               are added using the [::tcl::tm path add] command.
#
#   6.63   Mar. 28, 2013  Dennis R. LaBelle  1) Modified to support use of tcllib 1.16
#
proc main {} {
  global argv
  
  set mountpt {}
  set pkgType [lindex $argv 0]
  set scriptdirTK [file dirname [lindex $argv 1]]
  set scriptdir [file dirname $scriptdirTK]
  set zipdir [file dirname [lindex $argv 2]]
  set basedir [file dirname $zipdir]
  set baselen [string length $basedir]
  set autolist {}
  
  switch $pkgType {
    tcl - tk {
      set autolist {}
      set autolistTM {}
      set dirlist {}
  
      set majorDirs {thread2.8.4 sqlite3.25.2 tcl8 tcl8.6}
      set majorDirs [glob -tails -dir $scriptdir thread* sqlite* tdbc1* tdbcmysql1* tdbcodbc1* itcl*]
      file mkdir [file join $basedir modules]
      foreach pkg $majorDirs {
        set newdir [file join $basedir modules $pkg]
        set dirsrc [file join $scriptdir $pkg]
        if {[file isdir $dirsrc]} {
          if {$pkgType eq {tcl}} {
            # Do the actual copying only for the TCL case
            if [regexp {(thread|sqlite|itcl|tdbc|tdbcmysql|tdbcodbc)([0-9.]*)} $pkg egal modname modversion] {
              set initmodfile [open [file join $basedir init_modules.tcl] a]
              set dirsize [lindex [exec du -sb $dirsrc] 0]
              puts $initmodfile "set tec_modules_desc($modname) \[list \"$modname\" \"$modname\" \"$modversion\" \"TCL-Main-Module\" \"https://www.tcl.tk\" $dirsize\]"
              close $initmodfile
            }
            file delete -force $newdir
            puts "Copying $dirsrc to $newdir"
            file copy $dirsrc $newdir
          }
          lappend dirlist $newdir
        } else {
          puts "Could not find directory $dirsrc. Please install $pkg"
          exit 1
        }
      }

      set majorDirs [list "tcl8" "tcl8.6"]
      foreach pkg $majorDirs {
        set newdir [file join $basedir $pkg]
        set dirsrc [file join $scriptdir $pkg]
        if {[file isdir $dirsrc]} {
          if {$pkgType eq {tcl}} {
            # Do the actual copying only for the TCL case
            file delete -force $newdir
            puts "Copying $dirsrc to $basedir"
            file copy $dirsrc $basedir
          }
          lappend dirlist $newdir
        } else {
          puts "Could not find directory $dirsrc. Please install $pkg"
          exit 1
        }
      }
  
      if {$pkgType eq {tk}} {
        # Add the TK specific directories
        set majorDirs {tk8.6}
        foreach pkg $majorDirs {
          set newdir [file join $basedir $pkg]
          file delete -force $newdir
          set dirsrc [file join $scriptdir $pkg]
          if {[file isdir $dirsrc]} {
            puts "Copying $dirsrc to $basedir"
            file copy $dirsrc $basedir
            lappend dirlist $newdir
          } else {
            puts "Could not find directory $dirsrc. Please install $pkg"
            exit 2
          }
        }
        # Remove unnecessary subdirectories.
        if {$pkgType eq {tk}} {
          puts "Deleting [file join $basedir tk8.6 demos]"
          file delete -force [file join $zipdir demos]
        }
      }
  
      # Get list of subdirectories copied
      while {[llength $dirlist]} {
        set newdir [lindex $dirlist 0]
        set newlist {}
        if {[string length [glob -nocomplain $newdir/*.tm]]} {
          # The directory contains a TCL module.
          puts "Processing TCL module directory $newdir"
          lappend autolistTM [string range $newdir $baselen end]
        } else {
          puts "Processing TCL package directory $newdir"
          set pkgFile [file join $newdir pkgIndex.tcl]
          if {[file exists $pkgFile]} {
            lappend autolist [string range $newdir $baselen end]
          }
          set newlist [glob -nocomplain -types d [file join $newdir *]]
        }
        if {$newlist eq {}} {
          set dirlist [lrange $dirlist 1 end]
        } else {
          set dirlist [concat [lrange $dirlist 1 end] $newlist]
        }
      }
  
      # Add list of subdirectories to tcl_pkgPath in init.tcl file.
      puts "Modifying init.tcl with new module paths."
      set initFileSrc [file join $scriptdir tcl8.6 init.tcl]
      set initFileOrg [file join $basedir tcl8.6 init.org]
      set initfile [file join $basedir tcl8.6 init.tcl]
      file copy -force $initFileSrc $initFileOrg
      set fout2 [open $initfile w]
      # Extend auto_path to find standard TCL packages
      puts $fout2 "# Point to standard TCL packages"
      puts $fout2 {set ::tcl_pkgPath {}}
      foreach dir $autolist {
        set newpath [file join $mountpt $dir]
        puts $fout2 "lappend ::tcl_pkgPath \{$newpath\}"
      }
      puts $fout2 {}
      flush $fout2
  
      set fin2 [open $initFileOrg r]
      fcopy $fin2 $fout2
      close $fin2
  
      # Set up TCL module load paths.
      puts $fout2 {}
      puts $fout2 "# Point to wrapped tcl modules"
      puts $fout2 {foreach dtemp [::tcl::tm::path list] {::tcl::tm::path remove $dtemp}}
      foreach dir $autolistTM {
        set newpath [file join $mountpt $dir]
        puts $fout2 "::tcl::tm::path add \{$newpath\}"
      }
      puts $fout2 "# TCL library modifications completed."
      close $fout2
  
      file delete -force $initFileOrg
    }
    blt	{
      file delete -force $zipdir
      file copy $scriptdir $zipdir
  
      # Remove unnecessary subdirectories.
      file delete -force [file join $zipdir demos]
      file delete -force [file join $zipdir html]
    }
    default	{
      exit 1
    }
  }
}

main
puts "Library copy completed"
exit 0
