# Compiler Definitions
STDLIBS = -ldl -lauto -lm -lpthread -ObjC -framework CoreFoundation -framework CoreFoundation -framework Cocoa -framework Carbon -framework IOKit -framework CoreFoundation -framework QuartzCore
ARCH=x86_64-apple-darwin16

export CC=o64-clang -DSTATIC_BUILD=1 -DMAC_OSX_TCL=1 -D__APPLE__
export LD=$(ARCH)-ld
export AR=$(ARCH)-ar
export RANLIB=$(ARCH)-ranlib
export NM=$(ARCH)-nm

CFLAGS= -pipe -fPIC -DSTATIC_BUILD -DMACOS

ZIPOBJDIR    = $(PLATFORMDIR)/build/zip31c
OPT = -I$(PLATFORMDIR)/buildenv/include

LIBS = \
  $(PLATFORMDIR)/buildenv/lib/libtk$(TCLVERSIONDOT).a \
  $(PLATFORMDIR)/buildenv/lib/libtcl$(TCLVERSIONDOT).a \
  $(PLATFORMDIR)/buildenv/lib/libz.a

OBJSZIP = $(PLATFORMDIR)/zipmain.o \
  $(ZIPOBJDIR)/zipfile.o \
  $(ZIPOBJDIR)/zipup.o \
  $(ZIPOBJDIR)/fileio.o \
  $(ZIPOBJDIR)/util.o \
  $(ZIPOBJDIR)/globals.o \
  $(ZIPOBJDIR)/crypt.o  \
  $(ZIPOBJDIR)/ttyio.o \
  $(ZIPOBJDIR)/unix.o \
  $(ZIPOBJDIR)/crc32.o \
  $(ZIPOBJDIR)/zbz2err.o \
  $(ZIPOBJDIR)/deflate.o \
  $(ZIPOBJDIR)/trees.o

# All object code modules
#
OBJ = $(OBJSZIP) \
  $(PLATFORMDIR)/spritz.o \
  $(PLATFORMDIR)/freelib.o \
  $(PLATFORMDIR)/freewrap.o \
  $(PLATFORMDIR)/fwcrypt.o \
  $(PLATFORMDIR)/zvfs.o

$(PLATFORMDIR)/zipmain.o : zipmain.c
	$(CC) -c -I. -DUSE_ZIPMAIN -DUSE_ZLIB -DUNIX -DENABLE_ENTRY_TIMING -DUIDGID_NOT_16BIT -DLARGE_FILE_SUPPORT -DUNICODE_SUPPORT -DHAVE_DIRENT_H -DNO_PARAM_H -DHAVE_TERMIOS_H -DUNAME_R='"14.3.0"' -DUNAME_S='"Darwin"' -DUNAME_V='"Darwin Kernel Version 14.3.0: Mon Mar 23 11:59:05 PDT 2015; root:xnu-2782.20.48~5/RELEASE_X86_6"' -o $(PLATFORMDIR)/zipmain.o -c zipmain.c -I $(PLATFORMDIR)/build/zip31c/

$(PLATFORMDIR)/$(PROG)_bare$(EXE):	$(OBJ) $(LIBS)
	$(CC) $(LFLAGS) -o $(PLATFORMDIR)/$(PROG)_bare$(EXE) $(OBJ) $(LIBS) $(STDLIBS)
	$(ARCH)-strip $(PLATFORMDIR)/$(PROG)_bare$(EXE)
